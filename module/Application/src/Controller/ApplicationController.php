<?php
/**
 * Created by PhpStorm.
 * User: Gerlisson
 * Date: 24/03/2019
 * Time: 18:05
 */

namespace ApplicationController;

use GlobalsController\GlobalsController;
use RouterController\RouterController;
use HomeController\HomeController;
use CategoryController\CategoryController;

class ApplicationController
{
    public function render($get)
    {
        if (isset($get['p'])) {
            $get = explode("/", $get['p']);
        }

        include(__DIR__ . '/../view/head.phtml');
        include(__DIR__ . '/../view/header.phtml');

        $router = new RouterController($get);

        include(__DIR__ . '/../view/footer.phtml');
    }

    public function &factory($className)
    {
        require_once($className.'php');

        if(class_exists($className)) return new $className;

        die('Cannot create new "'.$className.'" class - includes not found or class unavailable.');
    }
}