<?php
/**
 * Created by PhpStorm.
 * User: Gerlisson
 * Date: 24/03/2019
 * Time: 19:00
 */

namespace CategoryController;


use GlobalsController\GlobalsController;
use Category;

class CategoryController extends GlobalsController
{
    public function __construct($get)
    {
        parent::__construct();

        if(! isset($get[1])){

            $this->listAll();

        }else{
            $this->addEdit($get);

        }

    }

    public function addEdit($get)
    {
        $data = array();

        $category = $this->getEm()->getRepository("Category")->find($get[1]);

        if(count($category)){
            $data['id'] = $category->getId();
            $data['name'] = $category->getName();
            $data['code'] = $category->getCode();
        }

        $this->setBd($data);

        include(__DIR__ . '/../view/category/add-edit.phtml');
    }

    public function listAll()
    {
        $data['categories'] = $this->getEm()->getRepository("Category")->findBy(array(), array('name' => 'asc'));

        $this->setBd($data);

        include(__DIR__ . '/../view/category/list.phtml');
    }

}