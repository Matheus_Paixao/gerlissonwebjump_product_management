<?php

/**
 * @Entity
 * @Table(name="category")
 */
class Category
{
    /**
     * @var integer
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /**
     * @var string
     * @Column(type="string", nullable=false)
     */
    private $name;

    /**
     * @var string
     * @Column(type="string", nullable=false)
     */
    private $code;

    /**
     * @var string
     * @Column(type="string", unique=true, nullable=false)
     */
    private $slug;

    /**
     * @var Product[]
     * @OneToMany(targetEntity="Product", mappedBy="categories")
     */
    private $products;

    /**
     * Category constructor.
     * @param string $name
     * @param string $code
     * @param string $slug
     */
    public function __construct($name, $code, $slug)
    {
        $this->name = $name;
        $this->code = $code;
        $this->slug = $slug;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return Product[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param Product[] $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }
}