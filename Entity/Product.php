<?php

/**
 * @Entity
 * @Table(name="product")
 */
class Product
{
    /**
     * @var integer
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /**
     * @var string
     * @Column(type="string", nullable=false)
     */
    private $name;

    /**
     * @var string
     * @Column(type="string", nullable=false)
     */
    private $sku;

    /**
     * @var decimal
     * @Column(type="decimal", nullable=false, precision=10, scale=2)
     */
    private $price;

    /**
     * @var string
     * @Column(type="text", nullable=false)
     */
    private $description;

    /**
     * @var int
     * @Column(type="integer",nullable=false, options={"default" = 0})
     */
    private $amount;

    /**
     * @var string
     * @Column(type="string", nullable=false)
     */
    private $image;

    /**
     * @var Category[]
     * @ManyToMany(targetEntity="Category")
     * @JoinTable(name="product_has_category",
     * joinColumns={@JoinColumn(name="product_id", referencedColumnName="id")},
     * inverseJoinColumns={@JoinColumn(name="category_id", referencedColumnName="id")}
     * )
     */
    private $categories;

    /**
     * @var string
     * @Column(type="string", unique=true, nullable=false)
     */
    private $slug;

    /**
     * Product constructor.
     * @param string $name
     * @param string $sku
     * @param decimal $price
     * @param string $description
     * @param int $amount
     * @param string $slug
     */
    public function __construct($name, $sku, $price, $description, $amount, $slug)
    {
        $this->name = $name;
        $this->sku = $sku;
        $this->price = $price;
        $this->description = $description;
        $this->amount = $amount;
        $this->slug = $slug;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return decimal
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param decimal $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return Category[]
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param Category[] $categories
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }
}
