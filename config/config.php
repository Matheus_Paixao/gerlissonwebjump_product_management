<?php
/**
 * @author  Gérlisson Paulino <gerlisson.paulino@gmail.com>
 * @license MIT
 */

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Sao_Paulo');

/* ** DEFINE DIRETÓRIO ** */
define('DS', DIRECTORY_SEPARATOR);
define('DIR_SITE', dirname(__DIR__) . DS);
define('DIR_CLASS', DIR_SITE . 'class' . DS);
define('DIR_DATAPROXIES', DIR_SITE . 'data/Proxies' . DS);
define('DIR_CONFIG', DIR_SITE . 'config' . DS);
define('DIR_VENDOR', DIR_SITE . 'vendor' . DS);
define('DIR_ENTITY', DIR_SITE . 'Entity' . DS);
define('DIR_MODULE', DIR_SITE . 'module' . DS);

@require_once("local.php");

define ('URL_MAIN', $dados['url_main']);
define ('AJAX', $dados['ajax']);

/* ** PASTAS ** */
define ('DIR_IMAGES', DIR_SITE . '/public/images/');
define ('DIR_PRODUCT', DIR_IMAGES . 'products/');

/*******************************/
define ('IMAGES', URL_MAIN . '/public/images/');
define ('PRODUCT', IMAGES . 'products/');

/********************** REGISTER / AUTOLOAD / ENTITIES ********************************/
require_once(DIR_CLASS . 'Registry.php');
require_once(DIR_VENDOR . 'autoload.php');

require_once(DIR_ENTITY . 'Category.php');
require_once(DIR_ENTITY . 'Product.php');

/********************** CONFIGURAÇÕES CONEXÃO *******************************/
$paths = array(DIR_ENTITY);
$isDevMode = false;

$dbParams = array(
    'driver' => 'pdo_mysql',
    'user' => $banco['datebase']['username'],
    'password' => $banco['datebase']['password'],
    'dbname' => $banco['datebase']['dbname'],
    'charset' => 'utf8',
    'driverOptions' => array(
        1002 => 'SET NAMES utf8'
    )
);
$doctrineConfig = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
//$doctrineConfig->setAutoGenerateProxyClasses(true);
$doctrineConfig->addFilter("ProductAvailableFilter", "ProductAvailableFilter");
//$doctrineConfig->setSQLLogger(new \Doctrine\DBAL\Logging\EchoSQLLogger());
$doctrineConfig->setProxyDir(DIR_DATAPROXIES);
$entityManager = EntityManager::create($dbParams, $doctrineConfig);

registry::register('entityManager', function () use ($dbParams, $doctrineConfig) {
    return EntityManager::create($dbParams, $doctrineConfig);
});

/******************************   CONTROLLER *****************************/
require(DIR_MODULE . 'Application/src/Controller/GlobalsController.php');
require(DIR_MODULE . 'Application/src/Controller/RouterController.php');
require(DIR_MODULE . 'Application/src/Controller/ApplicationController.php');
require(DIR_MODULE . 'Application/src/Controller/HomeController.php');
require(DIR_MODULE . 'Product/src/Controller/CategoryController.php');
require(DIR_MODULE . 'Product/src/Controller/ProductController.php');
