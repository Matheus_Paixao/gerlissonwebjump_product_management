$(document).ready(function(){

    $('#formProduct').submit(function(){
        var dados = new FormData($(this)[0]);
        var url_main = $(this).attr('data-urlmain');

        $('.form-message').html('<img src="'+url_main+'/public/assets/images/loading.gif" />');

        $.ajax({
            type: "POST",
            url: url_main + '/module/Product/src/ajax/products/products.php',
            dataType: 'json',
            data: dados,
            processData: false,
            contentType: false,
            success: function(data){

                if(data.success == true){
                    $('.form-message').html(data.message + '<a href="'+url_main+'/products" class="action back">Back</a><input class="btn-submit btn-action" type="submit" value="Save Product" />');

                    if(data.edit == true){
                        window.location.href = url_main + '/products';
                    }

                    setTimeout(function() {
                        $("#formProduct").trigger('reset');
                        $('.alert').fadeOut("slow");
                    }, 2500);
                }
                if(data.success == false){
                    $('.form-message').html(data.message + '<a href="'+url_main+'/products" class="action back">Back</a><input class="btn-submit btn-action" type="submit" value="Save Product" />');

                    setTimeout(function() {
                        $('.alert').fadeOut("slow");
                    }, 2500);
                }
            },
            error: function(request){
                $('.form-message').html(request.responseText + '<a href="'+url_main+'/products" class="action back">Back</a><input class="btn-submit btn-action" type="submit" value="Save Product" />');

                setTimeout(function() {
                    $('.alert').fadeOut("slow");
                }, 2500);
            }
        });
        return false;
    });

    $('#formCategory').submit(function(){
        var dados = $(this).serialize();
        var url_main = $(this).attr('data-urlmain');

        $('.form-message').html('<img src="'+url_main+'/public/assets/images/loading.gif" />');

        $.ajax({
            type: "POST",
            url: url_main + '/module/Product/src/ajax/categories/categories.php',
            dataType: 'json',
            data: dados,
            success: function(data){

                if(data.success == true){
                    $('.form-message').html(data.message + '<a href="'+url_main+'/categories" class="action back">Back</a><input class="btn-submit btn-action" type="submit" value="Save" />');

                    if(data.edit == true){
                        window.location.href = url_main + '/categories';
                    }

                    setTimeout(function() {
                        $("#formCategory").trigger('reset');
                        $('.alert').fadeOut("slow");
                    }, 2500);
                }
                if(data.success == false){
                    $('.form-message').html(data.message + '<a href="'+url_main+'/categories" class="action back">Back</a><input class="btn-submit btn-action" type="submit" value="Save" />');

                    setTimeout(function() {
                        $('.alert').fadeOut("slow");
                    }, 2500);
                }
            },
            error: function(request){
                $('.form-message').html(request.responseText + '<a href="'+url_main+'/categories" class="action back">Back</a><input class="btn-submit btn-action" type="submit" value="Save" />');

                setTimeout(function() {
                    $('.alert').fadeOut("slow");
                }, 2500);
            }
        });
        return false;
    });
});
function deleteCategory(url_main, id){
    if(confirm('Deseja Excluir?')){
        $.ajax({
            type: "GET",
            url: url_main + "/module/Product/src/ajax/categories/delete.php",
            dataType: 'json',
            data: {'id':id},
            success: function (data) {
                if (data.success == true) {
                    $('#del' + id).fadeOut("slow", function () {
                        $(this).remove();
                    });
                }
                if (data.success == false) {
                    alert(data.message);
                }
            },
            error: function (request) {
                alert(request.responseText);
            }
        });
    }
}
function deleteProduct(url_main, id){
    if(confirm('Deseja Excluir?')){
        $.ajax({
            type: "GET",
            url: url_main + "/module/Product/src/ajax/products/delete.php",
            dataType: 'json',
            data: {'id':id},
            success: function (data) {
                if (data.success == true) {
                    $('#del' + id).fadeOut("slow", function () {
                        $(this).remove();
                    });
                }
                if (data.success == false) {
                    alert(data.message);
                }
            },
            error: function (request) {
                alert(request.responseText);
            }
        });
    }
}